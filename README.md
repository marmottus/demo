A little demo with no size limit
---------------------------------

Music not by me.
You can find it here: [prettylightsmusic.com](http://prettylightsmusic.com)

Dependencies:
-------------

SDL, SDL-mixer, SDL-image, glew.
On a debian-like distribution:

    sudo apt-get install libsdl1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libglew1.6-dev
