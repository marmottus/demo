#ifndef SCENES_H
#define SCENES_H

# ifdef DEBUG
#  define WIDTH 800
#  define HEIGHT 600
# else
#  define WIDTH 1600
#  define HEIGHT 900
# endif

// For how long we stay on beat
# define BEAT_TIME 800.0

typedef unsigned int uint;

uint is_on_beat(uint time,
                  uint *pos);
int display(unsigned int time, unsigned int etime, uint b, float p);
void init_vbo(void);
void delete_vbo(void);

#endif
