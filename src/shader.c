#include "shader.h"
#include <stdio.h>
#include <stdlib.h>

typedef unsigned int uint;

char *loadShader(const char* filename)
{
  FILE* f = fopen(filename, "r");
  if (f == NULL)
  {
    perror(filename);
    exit(1);
  }
  fseek(f, 0, SEEK_END);
  long len = ftell(f);
  rewind(f);
  char *res = malloc((len+1)*sizeof (char));
  uint r = fread(res, len, 1, f);
  r = r;
  res[len] = '\0';
  fclose(f);
  return res;
}

static GLuint createShader(const char *strShaderFile, GLenum shaderType)
{
  GLuint shader = glCreateShader(shaderType);
  glShaderSource(shader, 1, &strShaderFile, NULL);

  glCompileShader(shader);

  GLint status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE)
  {
    GLint infoLogLength;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

    GLchar *strInfoLog = malloc((infoLogLength + 1) * sizeof (GLchar));
    glGetShaderInfoLog(shader, infoLogLength, NULL, strInfoLog);

    if (shaderType == GL_FRAGMENT_SHADER)
      fprintf(stderr, "Compile failure in fragment shader:\n%s\n", strInfoLog);
    else
      fprintf(stderr, "Compile failure in vertex shader:\n%s\n", strInfoLog);
    free(strInfoLog);
  }

  return shader;
}

static GLuint createProgram(GLuint vertexShader, GLuint fragmentShader)
{
  GLuint program = glCreateProgram();
  glAttachShader(program, vertexShader);
  glAttachShader(program, fragmentShader);
  glLinkProgram(program);

  GLint status;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  if (status == GL_FALSE)
  {
    GLint infoLogLength;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

    GLchar *strInfoLog = malloc(sizeof (GLchar) * (infoLogLength + 1));
    glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
    fprintf(stderr, "Linker failure: %s\n", strInfoLog);
    free(strInfoLog);
  }

  glDetachShader(program, vertexShader);
  glDetachShader(program, fragmentShader);

  return program;
}

void initProgram(GLuint *program,
                 const char *vertexStr,
                 const char *fragmentStr)
{
  GLuint vertexShader = createShader(vertexStr, GL_VERTEX_SHADER);
  GLuint fragmentShader = createShader(fragmentStr, GL_FRAGMENT_SHADER);
  *program = createProgram(vertexShader, fragmentShader);
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
}
