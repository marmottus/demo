#define NB_VERTEX  24

enum {DOWN=0, UP=1, LEFT=2, RIGHT=3, FRONT=4, BACK=5};
enum {SW=0, NW=1, NE=2, SE=3};
#define VERTEX_INDEX(face, pos) (face * 12 + pos * 3)

static GLfloat position[NB_VERTEX * 3] =
{
//DOWN
  1.0,  1.0, -1.0,
  1.0, -1.0, -1.0,
 -1.0, -1.0, -1.0,
 -1.0,  1.0, -1.0,

//UP
  1.0, -1.0,  1.0,
  1.0,  1.0,  1.0,
 -1.0,  1.0,  1.0,
 -1.0, -1.0,  1.0,

//LEFT
  1.0,  1.0, -1.0,
  1.0,  1.0,  1.0,
  1.0, -1.0,  1.0,
  1.0, -1.0, -1.0,

//RIGHT
 -1.0, -1.0, -1.0,
 -1.0, -1.0,  1.0,
 -1.0,  1.0,  1.0,
 -1.0,  1.0, -1.0,

//FRONT
 -1.0,  1.0, -1.0,
 -1.0,  1.0,  1.0,
  1.0,  1.0,  1.0,
  1.0,  1.0, -1.0,

//BACK
  1.0, -1.0, -1.0,
  1.0, -1.0,  1.0,
 -1.0, -1.0,  1.0,
 -1.0, -1.0, -1.0,
};

static GLfloat texture[NB_VERTEX * 2] =
{
 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,

 -1.0, -1.0,
  1.0, -1.0,
  1.0,  1.0,
 -1.0,  1.0,
};

static GLfloat normal[NB_VERTEX * 3] =
{
//DOWN
  0.0,  0.0, -1.0,
  0.0,  0.0, -1.0,
  0.0,  0.0, -1.0,
  0.0,  0.0, -1.0,

//UP
  0.0,  0.0,  1.0,
  0.0,  0.0,  1.0,
  0.0,  0.0,  1.0,
  0.0,  0.0,  1.0,

//LEFT
  1.0,  0.0,  0.0,
  1.0,  0.0,  0.0,
  1.0,  0.0,  0.0,
  1.0,  0.0,  0.0,

//RIGHT
 -1.0,  0.0,  0.0,
 -1.0,  0.0,  0.0,
 -1.0,  0.0,  0.0,
 -1.0,  0.0,  0.0,

//FRONT
  0.0,  1.0,  0.0,
  0.0,  1.0,  0.0,
  0.0,  1.0,  0.0,
  0.0,  1.0,  0.0,

//BACK
  0.0, -1.0,  0.0,
  0.0, -1.0,  0.0,
  0.0, -1.0,  0.0,
  0.0, -1.0,  0.0,
};

static GLubyte color[NB_VERTEX * 3] =
{
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
  255,   255,   255,
};

void draw_square(void)
{
  glBindBuffer(GL_ARRAY_BUFFER, buf[COLOR]);
  glBufferData(GL_ARRAY_BUFFER, sizeof (color), color, GL_STREAM_DRAW);
  glColorPointer(3, GL_UNSIGNED_BYTE, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, buf[POSITION]);
  glBufferData(GL_ARRAY_BUFFER, sizeof (position), position, GL_STREAM_DRAW);
  glVertexPointer(3, GL_FLOAT, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, buf[NORMAL]);
  glBufferData(GL_ARRAY_BUFFER, sizeof (normal), normal, GL_STREAM_DRAW);
  glNormalPointer(GL_FLOAT, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, buf[TEXTURE]);
  glBufferData(GL_ARRAY_BUFFER, sizeof (texture), texture, GL_STREAM_DRAW);
  glTexCoordPointer(2, GL_FLOAT, 0, 0);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  glDrawArrays(GL_QUADS, 0, NB_VERTEX);

  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_NORMAL_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
}
