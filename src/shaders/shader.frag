uniform int t;
uniform vec2 resolution;
uniform float beat_p;
float time = float(t)/1000.0;
uniform sampler2D tex;

bool is_green(vec3 c)
{
  return (c.g>c.r && c.g>c.b);
}

void main()
{
  vec2 q = gl_FragCoord.xy / resolution.xy;
  vec2 p = -1.0 + 2.0 * q;
  p.x *= resolution.x/resolution.y;
  vec3 col = gl_Color.rgb;
  vec4 texColor = vec4(1.0);

  if (is_green(col)) // Sky
  {
    col = vec3(max(0.0, p.y));
    if (mod(p.x+cos(p.y+time), 0.5) < 0.01 || mod(p.y+sin(p.x+time), 0.5) < 0.01)
      col = vec3(0.0);
  }
  else
  {
    if (beat_p > 0.0)
      col *= 1.0+col-col*beat_p;
    texColor = texture2D(tex,gl_TexCoord[0].st);
  }

  // vignetage
  col *= 0.25 + 0.75*pow(16.0*q.x*q.y*(1.0-q.x)*(1.0-q.y), 0.5);
  col *= 0.3 + 0.7*pow(16.0*q.x*q.y*(1.0-q.x)*(1.0-q.y), 0.3);

  gl_FragColor = vec4(col, 1.0);
  gl_FragColor *= texColor;
}
