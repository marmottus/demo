NAME = demo

CC = gcc

CFLAGS  = -std=c99 -Wall -Wextra -pedantic `sdl-config --cflags`

ifeq ($(shell uname), Linux)
LDFLAGS = -lm `sdl-config --libs` -lSDL_mixer -lSDL_image -lGLEW  -lGL -lGLU
endif

ifeq ($(shell uname), Darwin)
LDFLAGS = -lm `sdl-config --libs` -lSDL_mixer -lGLEW -lSDL_image -framework OpenGL
endif

CSRC  = $(addprefix src/,demo.c shader.c scenes.c)
CHDR  = $(addprefix src/,shader.h scenes.h)

.PHONY: clean distclean

all: $(NAME)

$(NAME): $(CSRC) src/square.c $(CHDR)
	$(CC) -D DEBUG $(CFLAGS) -g -ggdb $(CSRC) -o $(NAME) $(LDFLAGS)

prod: $(CSRC) $(CHDR)
	$(CC) $(CFLAGS) $(CSRC) -o $(NAME) $(LDFLAGS)

clean:
	rm -f $(NAME) core

distclean: clean
